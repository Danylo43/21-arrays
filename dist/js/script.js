"use stric";

//1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift"
//та обчисліть кількість рядків з довжиною більше за 3 символи.
//Вивести це число в консоль.
const words = ["travel", "hello", "eat", "ski", "lift"];
const getMass = words.filter((word) => word.length <= 3);

console.log(getMass);

//2. Створіть масив із 4 об'єктів, які містять інформацію про людей:
//{name: "Іван", age: 25, sex: "чоловіча"}. Наповніть різними даними.
//Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".
//Відфільтрований масив виведіть в консоль.

const person = [
  { name: "Іван", age: 25, gender: "чоловіча" },
  { name: "Тетяна", age: 28, gender: "жіноча" },
  { name: "Дмитро", age: 21, gender: "чоловіча" },
  { name: "Олена", age: 21, gender: "жіноча" },
];

const getPerson = person.filter((person) => person.gender === "чоловіча");
console.log(getPerson);

//3. Реалізувати функцію фільтру масиву за вказаним типом даних.
//(Опціональне завдання)
//- Написати функцію filterBy(), яка прийматиме 2 аргументи.
//Перший аргумент - масив, який міститиме будь-які дані,
//другий аргумент - тип даних.
//- Функція повинна повернути новий масив, який міститиме
//всі дані, які були передані в аргумент, за винятком тих,
//тип яких був переданий другим аргументом. Тобто якщо передати масив
//['hello', 'world', 23, '23', null], і другим аргументом передати
//'string', то функція поверне масив [23, null].

const filterBy = (arr, data) => {
  if (!Array.isArray(arr)) {
    console.warn("Помилка! Значення НЕ є массивом");
    return [];
  } 


  return arr.filter(item => typeof item === data);
};

const randomArray = ["hello", "world", 23, "23", null];

const result = filterBy(randomArray, 'string');

console.log(result);