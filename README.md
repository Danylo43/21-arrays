## 1. Опишіть своїми словами як працює метод forEach.

Метод **forEach** перебирає кожен елемент масиву від першого до останнього. Він завжди виконується для кожного елемента масиву,
тому, його не можна перерервати. Він завжди буде повертати `undefined`.

## 2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади.

**Методи, що мутують існуючий масив:**

- sort()

`let arr = [3, 1, 2];
arr.sort();`

- reverse() 

`let arr = [1, 2, 3];
arr.reverse();`

- pop()

`let arr = [1, 2, 3];
arr.pop();`

- push() 

`let arr = [1, 2, 3];
arr.push(4);`

- unshift()

`let arr = [1, 2, 3];
arr.unshift(0);`

**Методи, що повертають новий масив:**

- filter()

`let arr = [1, 2, 3, 4];
 let newArr = arr.filter(x => x > 2);`

 - flat()

 `let arr = [1, [2, 3], [4, 5]];
let newArr = arr.flat();`

- flatMap()

`let arr = [1, 2, 3];
let newArr = arr.flatMap(x => [x, x * 2]);`

- map()

`let arr = [1, 2, 3];
let newArr = arr.map(x => x * 2);`

- concat()

`let arr = [1, 2];
let newArr = arr.concat([3, 4]);`

## 3. Як можна перевірити, що та чи інша змінна є масивом?

 - 1) Метод Array.isArray()   
 
```
let arr = [1, 2, 3];
let notArr = "Hello, World!";

console.log(Array.isArray(arr));
console.log(Array.isArray(notArr)); 
```
 

- 2) Метод instanceof

```
let arr = [1, 2, 3];
let notArr = "Hello, World!";

console.log(arr instanceof Array);
console.log(notArr instanceof Array);
```


## 4. В яких випадках краще використовувати метод map(), а в яких forEach()?

**map()** використовується для ствоерння нового масиву, в якому кожен елемент є результатом 
застосування функції до відповідного елемента вихідного масиву.

**forEach()** використовується для виконання певної дії для кожного елемента масиву, але не 
повертає новий масив
